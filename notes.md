# Informatics

### C++ basics

##### Input & Output

- C style is faster, but can be sometimes tricky when you are reading strings

- C++ is slower (compilig time), but simple and easy.

- C++ compiling time can be optimized with `ios::sync_with_stdio(false)`

- Doesn't really matter, use whichever you wish to use.

  ```c++
  #include<iostream> // c++ library for file io
  #include<cstdio> // c library for file io -> faster
  using namespace std; // because we are importing functions from the standard template library // bad practice in general 
  
  int main(){
      // c++ style file io (easier)
      ios::sync_with_stdio(false);// makes c++ style faster
      int x, y;
      cin >> x >> y; // input
      cout << x << y; // output
      cout << "\n";
      
      // c style file io (faster compiling time)
      int x, y;
      scanf("%d %d", &x, &y); // input  %d -> declaring data type is an integer
      printf("%d %d\n") // output
       
  	return 0;
  }
  ```

  ![Screen Shot 2019-01-17 at 12.31.20 PM](/Users/user/Desktop/Screen Shot 2019-01-17 at 12.31.20 PM.png)

- Reading from and writing to external file: just add those two lines.

  ```c++
  int main(){
      freopen("filename.in", "r", stdin);
      freopen("filename.out", "w", stdout);
      
      ...
      ...
          
      return 0;
  }
  ```

##### Running C++ code

```bash
g++ std=c++11 file.cpp -o file  # compiles the code
./file # runs the code
```

##### Using Standard Template Library (STL)

- Is a collection of tools and functions that are optimized

- `#include<vector>` : Array (list) library. (used everytime).

  ```c++
  #include<vector>
  #include<iostream>
  using namespace std;
  
  int main(){
  	vector<int> a;
      vector<char> b;
      vector<float> c;
      vector<double> d;
      
      // initializing array (they all initialize to 0)
      a.resize(10);  // vector of size 10 of integers
      vector<int> e(10); // vector e of size 10 of integers
      
      // adding elements to vector
      a.push_back(5); // adds 5, index of 5 = 10;
      
      // print i'th element of vector
      cout << a[5];
      print("%d", a[5]);
      
      // 2-Dimensional vectors -> but don't think of them as matrices.
      // just vectors of vectors
      // go to loops first
      vector<int> twoDvector;
      twoDvector.resize(2);
      for(int i = 0; i < twoDvector.size(); i++){ // 2 by 5 // what do you think size() does?
          a[i].resize(5)
      }
      
      return 0;
  }
  ```

- `#include<string>` : string library 

  ```c++
  #include<string>
  #include<vector>
  using namespace std;
  
  int main(){
      String str = "lmao";
      vector<String> arrString;
      
      return 0;
  }
  ```

  

- `#include<algorithm> ` : common functions like sort() <- this is the only one I had to use but there should more useful stuff.

  ```c++
  #include<algorithm>
  #include<vector>
  using namespace std;
  
  struct greater{ // we'll talk about struct later
      template<class T>;
      bool operator()(T const &a, T const &b) const {return a > b;}
  };
  
  int main(){
      vector<int> v = {0, 9, 4, 7, 2, 1, 6};
      sort(v.begin(), v.end()); // ascending order
      sort(v.begin(), v.end(), greater()); // descending order
       
      return 0;
  }
  ```

  

- `#include<queue>`,  `#include<deque>` : Not really commonly used but good to know

  

##### More important stuff

- For loops (Classic)

  ```c++
  #include<iostream>
  #include<vector>
  using namespace std;
  
  int main(){
      for(int i = 0; i < 10; i++){
      	cout << i;   // prints 0123456789  
      }
      
      vector<int> v = {1, 3, 5, 2, 6};
      for(int i = 0; i < 10; i++){
          cout << v[i] << "\n";    // print i'th element in array v
          /* prints
          1
          3
          5
          2
          6
          */
      }
      
      //going through 2-D vectors
      vector<int> v = {{1, 2}, {3, 4}};
      for(int i = 0; i < v.size(); i++){
          for(int j = 0; j < v[i].size(); i++){
              cout << v[i][j] << " ";
          }
          cout << "\n";
      }
      
  	return 0;
  }
  ```

  

- For loops (Ranged-based)

  ```C++
  #include<iostream>
  #include<vector>
  using namespace std;
  
  int main(){
      vector<int> v = {1, 2, 3, 4};
      for(auto &x : v){
          cout << x << " "; // prints 1 2 3 4
      }
      
      vector<int> v = {{1, 2}, {3, 4}};
      for(auto &row : v){
          for(auto &column : row){
          	cout << column << " ";
              /*prints 
              	1 2
              	3 4
              */            
          }
          cout << "\n";
      }
      
      return 0;
  }
  ```

  

- Boolean Logic

  ```C++
  #include<vector>
  using namespace std;
  
  int main(){
      bool amIhappy = true;
      if(amIhappy){
          cout << "I'm happy";
      }
      if(!amIhappy){
          cout << "I'm not happy"
      }
      if(amIhappy == true){
          cout << "I'm happy";
      }
      if(amIhappy){
          cout << "I'm not happy";
      }
      
     int a = 4; int b = 3;
      if(a > b){ // also >, <, >=, <=, ==, !=
          cout << "hi";
      }
      
      return 0;
  }
  ```

- Little Bit of Object Oriented Programming

  ```c++
  #include <iostream>
  #include <vector>
  using namespace std;
  
  struct Shape{
      shape(int w, int h): width(w), height(h) {};
      int width;
      int height;
      void print_shape(){
          cout << width * height;
      }   
  }
  
  int main(){
      Shape bob(10, 5);
      bob.print_shape();
      
      vector<Shape> bob; // this is also legal
      
      return 0;
  }
  ```

  

  **This is the end of C++ Basics! Now we can learn more as we learn Algorithms and Data Structures**



###Complexity Analysis

https://www.cs.cmu.edu/~adamchik/15-122/lectures/complexity/complexity.pdf

- We'll only care about Big-O
- Something that's just good to know 
- We'll analyize Big-O's of the algorithms we use

### Data Structures - 1

1. **Stack**

2. **Queue**

3. **Priority Queue**

   

### Searching Algorithms - 1

1. **Complete Search**
2. **Binary Search**

### Greedy Algorithmn - 1



### Graph Theory - 1

1. **Representing Graphs**
2. **Breadth First Search**
3. **Depth First Search**
4. **Floodfill** 

### Prefix Sums



### Dynamic Programming - 1

1. **Top Down**

2. **Botton Up**

   



 





